package com.ICT311.Karla_Smith_1089222.MyReceipts;

import java.util.Date;
import java.util.UUID;

public class Receipt {
    private UUID mId;
    private String mTitle;
    private String mShopname;
    private String mComment;
    private Date mDate;


    public Receipt(){
        this(UUID.randomUUID());
    }

    public Receipt(UUID id){
        mId = id;
        mDate = new Date();

    }
    public UUID getId(){
        return  mId;
    }
    public String getTitle(){
        return mTitle;
    }
    public void setTitle(String title){
        mTitle = title;
    }
    public String getShopname() {
        return mShopname;
    }
    public void setShopname(String mShopname) {
        this.mShopname = mShopname;
    }
    public String getComment() {
        return mComment;
    }
    public void setComment(String mComment) {
        this.mComment = mComment;
    }
    public Date getDate() {
        return mDate;
    }


    public void setDate(Date date) {
        mDate = date;
    }


    public String getPhotoFilename() {
        return "IMG_" + getId().toString() + ".jpg";
    }

}
