package com.ICT311.Karla_Smith_1089222.MyReceipts;

import android.support.v4.app.Fragment;

public class ReceiptListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment(){
        return new ReceiptListFragment();
    }
}
