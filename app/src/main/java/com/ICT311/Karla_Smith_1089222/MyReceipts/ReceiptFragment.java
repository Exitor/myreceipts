package com.ICT311.Karla_Smith_1089222.MyReceipts;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static android.content.Context.LOCATION_SERVICE;

public class ReceiptFragment extends Fragment {
    private static final String ARG_RECEIPT_ID = "receipt_id";
    private static final String DIALOG_DATE = "DialogDate";
    private static final int REQUEST_DATE = 0;
    private static final int REQUEST_CONTACT = 1;
    private static final int REQUEST_PHOTO = 2;
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 123;
    public static final int GPS_PERMISSION_REQUEST_CODE = 345;

    private Receipt mReceipt;
    private File mPhotoFile;
    private EditText mTitleField;
    private EditText mShopnameField;
    private EditText mCommentField;
    private Button mDateButton;
    private Button mDeleteButton;
    private Button mLocationButton;
    private Button mReportButton;
    private ImageView mPhotoButton;
    private ImageView mPhotoView;
    private TextView mLocationTextView;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private Location GPSLocationData;

    public static ReceiptFragment newInstance(UUID receiptId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_RECEIPT_ID, receiptId);

        ReceiptFragment fragment = new ReceiptFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID receiptId = (UUID) getArguments().getSerializable(ARG_RECEIPT_ID);
        mReceipt = ReceiptLab.get(getActivity()).getReceipt(receiptId);
        mPhotoFile = ReceiptLab.get(getActivity()).getPhotoFile(mReceipt);

        //mLocationTextView = (TextView) mLocationTextView.findViewById(R.id.gps_coords);

        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, 0, 0, locationListener);
                GPSLocationData = location;
                //Log.d("Location: ",GPSLocationData.toString());
                //System.out.print("Location: " + GPSLocationData.toString());
                //textView.setText("Location: " + GPSLocationData.toString());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)  == PackageManager.PERMISSION_GRANTED)
        {
            //intentionally left blank
        } else {
            String[] permissionRequest = {Manifest.permission.ACCESS_FINE_LOCATION};
            requestPermissions(permissionRequest, GPS_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onPause(){
        super.onPause();

        ReceiptLab.get(getActivity())
                .updateReceipt(mReceipt);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_receipt, container, false);

        mLocationTextView = (TextView) v.findViewById(R.id.gps_coords);
        if (GPSLocationData != null) {
            mLocationTextView.setText("Location: "+GPSLocationData.getLatitude()+","+GPSLocationData.getLongitude());
        }


        mTitleField = (EditText) v.findViewById(R.id.receipt_title);
        mTitleField.setText(mReceipt.getTitle());
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // intentionally blank
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mReceipt.setTitle(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
                // intentionally blank
            }
        });
        mShopnameField = (EditText) v.findViewById(R.id.shop_name);
        mShopnameField.setText(mReceipt.getShopname());
        mShopnameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // intentionally blank
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mReceipt.setShopname(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
                // intentionally blank
            }
        });
        mCommentField = (EditText) v.findViewById(R.id.comments);
        mCommentField.setText(mReceipt.getComment());
        mCommentField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // intentionally blank
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mReceipt.setComment(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
                // intentionally blank
            }
        });
        mDateButton = (Button) v.findViewById(R.id.receipt_date);
        updateDate();
        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(mReceipt.getDate());
                dialog.setTargetFragment(ReceiptFragment.this, REQUEST_DATE);
                dialog.show(manager, DIALOG_DATE);
            }
        });

        mReportButton = (Button) v.findViewById(R.id.receipt_report);
        mReportButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, getReceiptReport());
                i.putExtra(Intent.EXTRA_SUBJECT,
                        getString(R.string.receipt_report_subject));
                i = Intent.createChooser(i, getString(R.string.send_report));
                startActivity(i);
            }
        });

        mDeleteButton = (Button) v.findViewById(R.id.delete_receipt);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UUID receiptId = mReceipt.getId();
                ReceiptLab.get(getActivity()).deleteReceipt(receiptId);
                Toast.makeText(getActivity(), R.string.toast_delete_receipt, Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        });

        //GPS BUTTON
        // TEST SANFRANSICO "geo:37.7749,-122.4194"
        // TEST USC -26.7180177,153.0610553
        mLocationButton = (Button) v.findViewById(R.id.location_button);
        mLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Default to USC
                Uri gmmIntentUri = Uri.parse("geo:-26.7180177,153.0610553");
                if (GPSLocationData != null)
                {
                    gmmIntentUri = Uri.parse("geo:"+GPSLocationData.getLatitude()+","+GPSLocationData.getLongitude());
                }
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            }
        });

        final Intent pickContact = new Intent(Intent.ACTION_PICK,
        ContactsContract.Contacts.CONTENT_URI);



        final PackageManager packageManager = getActivity().getPackageManager();

        mPhotoButton = (ImageButton) v.findViewById(R.id.receipt_camera);
        final Intent captureImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        boolean canTakePhoto = mPhotoFile != null &&
                captureImage.resolveActivity(packageManager) != null;
        mPhotoButton.setEnabled(canTakePhoto);
        mPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = FileProvider.getUriForFile(getActivity(),
                        "com.ICT311.Karla_Smith_1089222.MyReceipts.fileprovider",
                        mPhotoFile);
                captureImage.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                List<ResolveInfo> cameraActivities = getActivity()
                        .getPackageManager().queryIntentActivities(captureImage,
                                PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo activity : cameraActivities) {
                    getActivity().grantUriPermission(activity.activityInfo.packageName,
                            uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }

                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)  == PackageManager.PERMISSION_GRANTED)
                {
                    startActivityForResult(captureImage, REQUEST_PHOTO);
                    getActivity().finish();
                } else {
                    String[] permissionRequest = {Manifest.permission.CAMERA};
                    requestPermissions(permissionRequest, CAMERA_PERMISSION_REQUEST_CODE);
                }
            }

            //@Override
            public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResult){
                ReceiptFragment.super.onRequestPermissionsResult(requestCode, permissions, grantResult);
                if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
                    if (grantResult[0] == packageManager.PERMISSION_GRANTED) {
                        startActivityForResult(captureImage, REQUEST_PHOTO);
                    } else {
                        Toast.makeText(getActivity(), R.string.toast_camera_permission, Toast.LENGTH_LONG).show();
                    }
                }
                if (requestCode == GPS_PERMISSION_REQUEST_CODE) {
                    if (grantResult[0] == packageManager.PERMISSION_GRANTED) {
                        //blank
                    } else {
                        Toast.makeText(getActivity(), R.string.gps_permission, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        mPhotoView = (ImageView) v.findViewById(R.id.receipt_photo);
        updatePhotoView();

        return v;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode != Activity.RESULT_OK){
            return;
        }
        if (requestCode == REQUEST_DATE){
            Date date = (Date) data
                    .getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            mReceipt.setDate(date);
            updateDate();
        } else if (requestCode == REQUEST_CONTACT && data != null) {
            Uri contactUri = data.getData();

// Specify which fields you want your query to return
// values for
            String[] queryFields = new String[] {
                    ContactsContract.Contacts.DISPLAY_NAME
            };
// Perform your query - the contactUri is like a "where"
// clause here
            Cursor c = getActivity().getContentResolver()
                    .query(contactUri, queryFields, null, null, null);
            try {
// Double-check that you actually got results
                if (c.getCount() == 0) {
                    return;
                }
// Pull out the first column of the first row of data -
// that is your suspect's name
                c.moveToFirst();
            } finally {
                c.close();
            }
        } else if (requestCode == REQUEST_PHOTO){
            Uri uri = FileProvider.getUriForFile(getActivity(),
                    "com.ICT311.Karla_Smith_1089222.MyReceipts.fileprovider",
                    mPhotoFile);
            getActivity().revokeUriPermission(uri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            updatePhotoView();
        }
    }


    private void updateDate() {
        mDateButton.setText(mReceipt.getDate().toString());
    }
    private String getReceiptReport() {

        String dateFormat = "EEE, MMM dd";
        String dateString = DateFormat.format(dateFormat,
                mReceipt.getDate()).toString();
        String report = getString(R.string.receipt_report,
                mReceipt.getTitle(), mReceipt.getShopname(), mReceipt.getComment(), dateString);
        return report;
    }
    private void updatePhotoView() {
        if (mPhotoFile == null || !mPhotoFile.exists()) {
            mPhotoView.setImageDrawable(null);
        } else {
            Bitmap bitmap = PictureUtils.getScaledBitmap(
                    mPhotoFile.getPath(), getActivity());
            mPhotoView.setImageBitmap(bitmap);
        }
    }
}
