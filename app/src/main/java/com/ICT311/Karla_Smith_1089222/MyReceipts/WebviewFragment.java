package com.ICT311.Karla_Smith_1089222.MyReceipts;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class WebviewFragment extends AppCompatActivity {
    private static final String ARG_URL = "help_page_url";

    private Uri mUri;
    private WebView mWebView;
    private String mId;
    private String mOwner;

    public static WebviewFragment newInstance(Uri uri){
        Bundle args = new Bundle();
        args.putParcelable(ARG_URL, uri);

        WebviewFragment fragment = new WebviewFragment();
        //fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    //testing
    public String getOwner(){
        return mOwner;
    }

    public void setmOwner(String mOwner) {
        this.mOwner = mOwner;
    }

    public Uri getmUri() {
        return mUri.parse("https://en.wikipedia.org/wiki/receipt")
                .buildUpon()
                .appendPath(mOwner)
                .appendPath(mId)
                .build();
    }
    //end test

    //@Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_webview, container, false);

        mWebView = (WebView) v.findViewById(R.id.webview);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.loadUrl("https://en.wikipedia.org/wiki/Receipt");

        return v;
    }


}
