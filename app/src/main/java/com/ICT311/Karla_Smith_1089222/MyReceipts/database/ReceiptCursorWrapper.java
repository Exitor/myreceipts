package com.ICT311.Karla_Smith_1089222.MyReceipts.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.ICT311.Karla_Smith_1089222.MyReceipts.Receipt;

import java.util.Date;
import java.util.UUID;

public class ReceiptCursorWrapper extends CursorWrapper {
    public ReceiptCursorWrapper(Cursor cursor){
        super(cursor);
    }
    public Receipt getReceipt() {
        //Reading from table column
        String uuidString = getString(getColumnIndex(ReceiptDbSchema.ReceiptTable.Cols.UUID));
        String title = getString(getColumnIndex(ReceiptDbSchema.ReceiptTable.Cols.TITLE));
        String shopname = getString(getColumnIndex(ReceiptDbSchema.ReceiptTable.Cols.SHOPNAME));
        String comment = getString(getColumnIndex(ReceiptDbSchema.ReceiptTable.Cols.COMMENT));
        long date = getLong(getColumnIndex(ReceiptDbSchema.ReceiptTable.Cols.DATE));


        Receipt receipt = new Receipt(UUID.fromString(uuidString));
        receipt.setTitle(title);
        receipt.setShopname(shopname);
        receipt.setComment(comment);
        receipt.setDate(new Date(date));

        return receipt;

    }
}
